# Recognition Suite Development Environment Setup on Windows


## Informations

Before installing / upgrading any of the following components, the target system *must* be up to date.

Also, it's important to understand why a compilation is required for all dependencies of PQR Recognition Suite.

* Self-contained compatibility over any system.
* Same version of CRT for any component of a given system (For instance, it's dangerous to mix RELibrary compiled with a Multithreaded Debug DLL and OpenCV compiled with a static CRT).
* Control of the versions of every single packages over the system-provided packages.
As some of these steps can be very long, they are required, and should only be done once in a while (E.g. only when changing compiler, or when upgrading Qt).


## Compilers / Toolchains

In this order, install:

* Visual Studio Express 2012,
* Visual Studio Express 2012 Update 4

Then, run Windows Update and make sure everything related. You *must* restart when prompted.

Once everything is installed, create shortcuts for various prompts. We have 4 of them, as follow:

* `VS2012 x64` with target `%comspec% /k ""C:\Program Files (x86)\Microsoft Visual Studio 11.0\VC\vcvarsall.bat"" x86_amd64`
* VS2012 x86 with target `%comspec% /k ""C:\Program Files (x86)\Microsoft Visual Studio 11.0\VC\vcvarsall.bat"" x86`

If you plan to debug application on the same machine, you may want to install Windows SDK 8.0 as well (Only pick the "Debugging Tools" when the installer runs).


## Tesseract + Dependencies

As of today, we are building Tesseract 3.02 from source, for both 32 and 64 bit architectures.

To do so, it is required that a fresh version of leptonica 1.68 is built, using the same compiler and linking against the same CRT versions that tesseract will.

To be able to successfully compile leptonica, it is required that it's dependencies are also resolved for all target architectures. Those dependencies are:

* zLib 1.25
* libpng 1.43
* libjpeg 8c
* libtiff 3.9.4
* giflib 4.1.6

This document will not go into the specifics on how to achieve this, as a package regroupping everything we compiled using Visual Studio 2012 is kept as a reusable archive for 32 and 64 bit architectures.

Compiling tesseract from source won't be a detailed part of this document neither, since an archive containing everything we compiled using Visual Studio 2012 is available.


## OpenCV

* Download the OpenCV Source Tree (As of today, we are using OpenCV 2.4.8).
* Unpack the source code in `C:\OpenCV248`.

Then, start CMake-GUI and, for an x86 build:

* At any time, when CMake-GUI asks for a toolchain, select the MSVC2012 appropriate one (Win32 or x64).
* For the "Where is the source code" option, make it read `C:/OpenCV248/sources`.
* As of the `Where to build the binaries` option, select `C:/OpenCV248/Builds/x86`.
* Hit the `Configure` button, and wait for CMake to finish.
* In the resulting configuration option, make sure the static linking to CRT is disabled (`BUILD_WITH_STATIC_CRT=0`).
* Optionnaly, disable the tests and performance tests (`BUILD_TESTS` / `BUILD_PERF_TESTS`).
* Hit the `Configure` button again to ensure options are taken into account, then hit `Generate`.

Then, using the appropriate MSVC prompt, navigate to the shadow build directory (E.g. `C:/OpenCV248/Builds/x86` if using the x86 toolchain).

Building the release version of the library is done with issuing the command `msbuild /t:Build /p:Configuration=Release`.

Once done, it's up to you to build the debug version of the library, with the command `msbuild /t:Build /p:Configuration=Debug`.

Once built, issuing the command `msbuild Install.vcxproj /t:Build /p:Configuration=Debug /v:m` will move everything important in the `install` directory (E.g. `C:\OpenCV248\Builds\x86\install`).

After building any for any number of configurations and architectures, merging the "install" folders will make a self-contained SDK for linking against OpenCV. For instance, after successfully building for x86 / x64 in Debug / Release modes, the `install` directory internal structure looks like this:

```
+ install
  + include       (Those are common headers, for any configuration and for any architecture)
  + x64           (This is the build for the amd64 architecture)
  + x86           (This is the build for the amd64 architecture)
  + etc...
```

Then, in each architecture-specialized directory, the following structure can be found:

```
+ vc11        (Specialized build by VC2012)
  + bin       (Contains the actual binaries of the distribution, DLLs and EXEs)
  + lib       (Contains import libraries required for dynamic linking against binary libraries)
```

In the last two folders, any file suffixed with a `d` means that the file is the debug version of the binary.

For the 64 bit build, renew from CMake step 1, but substitute any occurence of `x86` with `x64`.

Once built, the `C:\OpenCV248` directory can be deleted.


## Qt SDK

### Perl

Go to ActiveState Perl and get the 5.x version (We are using the 5.14.4 version) installer.

Once the installer asks what packages to install, just pick Perl, and deselect everything else.

The destination folder should be the default (On a 64 bit system, it's "C:\Perl64").

Then, make sure the binary perl folder is in the system PATH variable (Perl sites modules should be removed from the system PATH variable).


### Qt Creator

Use the QtCreator Installer executable and pick `C:\QtSDK\QtCreator\x.x.x` for it's destination.

Make sure that the version numbers match the installer being used (E.g. `C:\[...]\3.0.0`).


### Qt Installer Framework

Use the QtIFW 1.4 Installer executable and:

* Pick `C:\QtSDK\QtIFW\1.x.x` for it's destination.
* Make sure that the version numbers match the installer being used (E.g. `C:\[...]\1.4.0`).


### Qt SDK Source Tree

Unzip the Qt Everywhere 4.8.5 source tree in `C:\QtSDK\Qt\4.8.5\Source`, so all files should be laid directly in this folder (E.g. `C:\QtSDK\Qt\4.8.5\Source\mkspecs`)


### Qt SDK Build Trees

Create a folder structure like this:

```
+ C:
  + QtSDK
    + Qt
      + 4.8.5
        + tmp32           (Temporary shadow-build location for any 32-bit related build)
        + tmp64           (Temporary shadow-build location for any 64-bit related build)
        + win-msvc2012-32 (Final build directory for the Qt SDK 32-bit version)
          + mkspecs       (Copy-pasted from the vanilla Source directory)
        + win-msvc2012-64 (Final build directory for the Qt SDK 64-bit version)
          + mkspecs       (Copy-pasted from the vanilla Source directory)
```

Then, building can start. Use the appropriate prompt for each version (x86 for the 32-bit tree, and x64 for the 64-bit tree). For instance, building the 64-bit tree would look like something like this:

```bash
cd C:\QtSDK\Qt\4.8.5\tmp64
..\Source\configure ^
        -prefix C:\QtSDK\Qt\4.8.5\win-msvc2012-64 ^
        -platform win32-msvc2012 ^
        -opensource ^
        -debug-and-release ^
        -fast ^
        -no-cups ^
        -no-webkit ^
        -no-phonon ^
        -no-opengl ^
        -no-openvg ^
        -no-multimedia ^
        -no-qt3support ^
        -no-accessibility ^
        -no-phonon-backend ^
        -nomake demos ^
        -nomake examples ^
        -nomake docs ^
        -nomake translations ^
        -qt-zlib ^
        -qt-libpng ^
        -qt-libmng ^
        -qt-libtiff ^
        -qt-libjpeg
```

After issuing this command, you must accept the opensource license.

Configuration of the shadow tree will then occur. Once done, run "nmake" to build the shadow build, then `nmake install` to move everything required to the `win-msvc2012-64` folder.

Once these steps are done, the `tmp64` folder can be removed.
