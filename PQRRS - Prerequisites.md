# Recognition Suite Pre-Requisites

## Supported Operating Systems

The Recognition Suite software toolkit as well as its runtime are compatible and compilable usins any of the following operating systems:

- Mac OS X (Tiger up to Yosemite),
- Windows (Starting from XP up to Seven),
- Linux (Major distros such as Ubuntu 14 or CentOS are supported).


## Compilation Environment

### Overview

To be able to compile the Recognition Suite toolkit and its runtime, it is recommended to conform to the following guidelines:

- Compile Qt 4.8.6 OpenSource without as few external dependencies as possible.
- Compile OpenCV using the bare minimum modules.
- Compile any satellite / plugin libraries on a host environment as close as the production environment will be.

The following IDE / Toolchains could be used to compile the Recognition Suite toolkit and its runtime:

- Qt Creator 3.4 can be used.
- On Windows, it's highly recommended to use MS Visual Studio to compile any external libraries prior to compile any of the Recognition Suite binary.


### Supported Compilers

#### Mac OSX

The standard LLVM-GCC binary that ships with the operating system is sufficient for compiling the entire framework as well as their dependencies.

Using Homebrew is highly recommended for external / satellite plugins (OpenCV, Tesseract, ZBar, etc).


#### Unix / Linux

The standard GCC that ships with the distro will be able to compile the whole framework as well as its satellite dependencies.

It is not recommended to use any default version of Qt, OpenCV or Tesseract. Instead, they should be compiled from source, allowing non-standard options such as relying on embeded image processing libraries, etc.


#### Windows

The standard Microsoft C / C++ Compiler that ships with any recent version of Microsoft Visual Studio is able to compile most of the framework as well as its dependencies.

For some plugins, a full copy of Microsoft Visual Studio will be required, as some of the dependencies only ship with Microsoft Solution Files (`.sln`).


### Deploying the Framework and its Runtime

The source code includes various Qt Creator projects that come pre-configured with local deployment strategies. Most of the time, the path /opt/PQRRS will be used.

As of today, its recommended to use a Unix / Linux compatible system for production environment, but the Windows versions of the binaries have been tested without any problem.

Any file generated with the Recognition Suite toolkit is compatible accross operating systems, as the generated data uses a unified versioning and encoding regarding the data types embeded within it.


## Guides

### Qt SDK

As Qt could be cumbersome to compile, it could be usefull to rely on the procedure below. It is designed to function with Linux, but could easilly be adapted to any other operating system.

Given a folder architecture which root resides in `/opt`, the following folder layout should be created:

```
/opt
  /QtSDK
    /Qt
      /4.8.6
        /configure.sh
        /Source
```

The Qt 4.8.6 source code should be extracted directly under the `/opt/QtSDK/Qt/4.8.6/Source` directory, with the purpose of shallow-builds (Multiple builds could use the same source code folder without any changes to it, allowing multiple versions of the same source to be built with different options).

The `configure.sh` script could look like so:

~~~sh
#!/bin/sh

ARCH=$1
echo "Will build for $ARCH."
mkdir $1
../Source/configure \
        -prefix /opt/QtSDK/Qt/4.8.6/$ARCH \
        -platform $ARCH \
        -opensource \
        -release \
        -fast \
        -no-cups \
        -no-webkit \
        -no-phonon \
        -no-qt3support \
        -no-phonon-backend \
        -nomake demos \
        -nomake examples \
        -nomake docs \
        -nomake translations
        -qt-zlib \
        -qt-libpng \
        -qt-libmng \
        -qt-libtiff \
        -qt-libjpeg
~~~

This script could then be called like so:

~~~sh
cd /opt/QtSDK/Qt/4.8.6
mkdir tmp
mkdir linux-g++
cd tmp
../configure linux-g++
~~~

Once the configure process is done, building, installing and then cleaning the artifacts could be done like so:

~~~sh
cd /opt/QtSDK/Qt/4.8.6/tmp
make && sudo make install
cd ..
rm -rf tmp
~~~

The directory structure would then be changed to this:

```
/opt
  /QtSDK
    /Qt
      /4.8.6
        /configure.sh
        /Source
        /linux-g++
```

At this point, `/opt/QtSDK/Qt/4.8.6/linux-g++` would then contain a release version of the Qt binaries ready for use / deployment.


### Tesseract

Prior to compiling tesseract, it is required to build its main dependency: Leptonica.

On Linux, building Leptonica is trivial, and should not take more steps than the usual `./configure`, `make` and `sudo make install`.

On Windows it could be a little bit tricky for a developer who is not familiar with unix build system. A good guide on how to build Leptonica can be found [here](http://www.leptonica.com/source/README.html).


### ZBar

Compiling ZBar should be achieved by following its [Official Wiki](https://github.com/scs/leanXcam/wiki/Compiling-zbar-for-leanXcam#Compilation_steps).


### LibDMTX

Compiling LibDMTX under Linux is a trivial task. For Windows directions, please refer to the LibDMTX [Official Website Section](http://libdmtx.wikidot.com/libdmtx-on-windows-using-visual-studio).
