#ifndef SC_SCLibrary_h
#define SC_SCLibrary_h

#include <QtGlobal>
#include "SCTypes.h"

#ifdef Q_OS_WIN
#include <windows.h>
extern "C" {
    SCLibOpt    qint32              WINAPI      DllMain                     (HINSTANCE hInstance, DWORD dwReason, LPVOID);
}
#endif

extern "C" {
    /*! \fn char const* SCVersion ()
     * \brief Allows the retrieval of the current SCLibrary binary version as a string.
     * \return A char* describing the version. */
    SCLibOpt    void const*         SCCConv     SCVersion                   ();
    /*! \fn char const* SCStaticInitializer ()
     * \brief Globally initializes the SCLibrary process, and has to be called only one time before doing anything else. */
    SCLibOpt    void                SCCConv     SCStaticInitializer         ();

    /*! \fn void* SCInstanciate (void const *devConfig)
     * \brief Creates a Scanner Controller instance based on a Device Configuration structure.
     * \param[in] licenseFilename A string containing the full path of the license file (Eg. "/home/me/.PQRRSLicense.lic").
     * \param devConfig A pointer to a Device Configuration structure object.
     * \return A handle to a Scanner Controller object that can be later be used with SCLibrary API. */
    SCLibOpt	void*               SCCConv     SCInstanciate               (char const *licenseFilename, void const *devConfig);

    /*! \fn quint32 SCInitialize (void *scHandle)
     * \brief Initializes a Scanner Controller instance.
     * \param[in] scHandle A Scanner Controller handle previously allocated with the SCInstanciate() function.
     * \return 0 when an error has occured, 1 if the operation was successfull. */
    SCLibOpt	qint32              SCCConv     SCInitialize                (void *scHandle);
    /*! \fn qint32 SCConfigure (void *scHandle, void const *config).
     * \brief Configures a Scanner Controller given a Configuration structure.
     * \param[in] scHandle A Scanner Controller handle previously allocated with the SCInstanciate() function that will be configured.
     * \param[in] config The Configuration object that will be used.
     * \return 0 when an error has occured, 1 if the operation was successfull. */
    SCLibOpt	qint32              SCCConv     SCConfigure                 (void *scHandle, void const *config);
    /*! \fn quint32 SCDelete (void *scHandle)
     * \brief Releases a previously allocated Scanner Controller.
     * \param[in] scHandle A Scanner Controller handle previously allocated with the SCInstanciate() function that will be destroyed. */
    SCLibOpt	void                SCCConv     SCDelete                    (void *scHandle);

    /*! \fn qint32 SCErrorCode (void *scHandle, quint8 reset).
     * \brief Returns the last error that occured in the given Scanner Controller instance, or 0 if none.
     * \param[in] scHandle A Scanner Controller handle previously allocated with the SCInstanciate() function that will be inspected.
     * \param[in] reset 1 to reset the last error, 0 to keep it.
     * \return An error code describing the last problem that occured in the Scanner Controller, if any. */
    SCLibOpt	qint32              SCCConv     SCErrorCode                 (void *scHandle, quint8 reset=true);

    /*! \fn qint32 SCStartProcessing (void *scHandle, quint8 block).
     * \brief Start the processing pass for the given Scanner Controller instance, either synchroneously or asynchroneously.
     * \param[in] scHandle A Scanner Controller handle previously allocated with the SCInstanciate() function that will be used to handle the pass.
     * \param[in] block To use this call synchroneously (Blocking way), use 1. Else, use 0 to immediattelly return and use callbacks to control the flow of the pass.
     * \return 0 when an error has occured, 1 if starting the pass was successfull. */
    SCLibOpt	qint32              SCCConv     SCStartProcessing           (void *scHandle, quint8 block=false);
    /*! \fn qint32 SCStopProcessing (void *scHandle).
     * \brief Stop the processing pass for the given Scanner Controller instance.
     * \param[in] scHandle A Scanner Controller handle previously allocated with the SCInstanciate() function for which the pass will be stopped.
     * \return 0 when an error has occured, 1 if stopping was successfull. */
    SCLibOpt	void                SCCConv     SCStopProcessing            (void *scHandle);

    SCLibOpt    quint32             SCCConv     SCDocumentKeyAndStringSize  (void *scHandle, quint32 docId, quint32 offset, quint32 &keySize, quint32 &valSize);
    /*! \fn qint32 SCDocumentStringAndKey (void *scHandle, quint32 docId, quint32 offset, quint8 *outKey, quint8 *outVal).
     * \brief Nothing
     * \param[in] scHandle Nothing
     * \param[in] docId Nothing
     * \param[in] offset Nothing
     * \param[out] outKey Nothing
     * \param[out] outVal Nothing
     * \return Nothing */
    SCLibOpt	quint32             SCCConv     SCDocumentKeyAndString      (void *scHandle, quint32 docId, quint32 offset, quint8 *key, quint8 *val);
}

#endif
