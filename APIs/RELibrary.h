#ifndef RE_RELibrary_h
#define RE_RELibrary_h

#include "RETypes.h"

#ifdef Q_OS_WIN
#include <windows.h>
extern "C" {
    RELibOpt    qint32      WINAPI      DllMain                         (HINSTANCE hInstance, DWORD dwReason, LPVOID);
}
#endif

extern "C" {
    /*! \fn char const* REVersion ()
     * \brief Allows the retrieval of the current RELibrary binary version as a string.
     * \return A char* describing the version. */
    RELibOpt    char const* RECConv     REVersion                       ();
    /*! \fn char const* REClientBinaryVersion ()
     * \fn Allows the retrieval of the minimum required version of a Client Binary package for RELibrary to use.
     * \return A char* describing the required Client Binary version. */
    RELibOpt    char const* RECConv     REClientBinaryVersion           ();

    /*! \fn char const* REStaticInitializer ()
     * \brief Globally initializes the RELibrary process, and has to be called only one time before doing anything else. */
    RELibOpt    void        RECConv     REStaticInitializer             ();

    /*! \fn void* RECreateEngine ()
     * \brief Instanciates a new Recognition Engine object, and returns a handle allowing to use it later.
     * \param[in] licenseFilename A string containing the full path of the license file (Eg. "/home/me/.PQRRSLicense.lic").
     * \return A handle to the newly allocated Recognition Engine object, or 0 if an error occured. */
    RELibOpt	void*       RECConv     RECreateEngine                  (char const *licenseFilename);
    /*! \fn quint32 RELoadEngineClientBinary (void *reHandle, char const *clientBinaryPath)
     * \brief Loads a Client Binary data file into a Recognition Engine instance, and returns the operation result.
     * \param[in] reHandle A Recognition Engine handle previously allocated with the RECreateEngine() function.
     * \param[in] clientBinaryPath A string containing the absolute or relative path of the Client Binary file to be loaded into the Recognition Engine.
     * \return 0 when an error has occured, 1 if loading was successfull. */
    RELibOpt    quint32     RECConv     RELoadEngineClientBinary        (void *reHandle, char const *clientBinaryPath);
    /*! \fn quint32 REDeleteEngine (void *reHandle)
     * \brief Releases a previously allocated Recognition Engine.
     * \param[in] reHandle A Recognition Engine handle previously allocated with the RECreateEngine() function.
     * \return 0 when an error has occured, 1 if loading was successfull. */
    RELibOpt	quint32     RECConv     REDeleteEngine                  (void *reHandle);
    /*! \fn void* RECreateDocument ()
     * \brief Creates a Recognition Document to be used later with an instance of a Recognition Engine.
     * A Recognition Document can have a type as well attachements (Pictures, strings, etc). Once a recognition document has been
     * created, and at least one picture has been attached to it, it is possible to either let the Recognition Engine try to
     * guess it's type or define it programmatically. Once the Recognition Document's type has been defined, the data recognition
     * itself can then occur using the RERecognizeDocumentData() function.
     * \return A void* pointer to the newly created Recognition Document instance, or 0 if an error occured. */
    RELibOpt    void*       RECConv     RECreateDocument                ();
    /*! \fn quint32 REDeleteDocument (void *rdHandle)
     * \brief Releases a Recognition Document instance as well as any resource it relates.
     * After type detection and recognition has been performed on a document, it needs to be released so any memory allocation
     * related to that object is freed.
     * Please note that any image memory allocated with the REGetDocumentImageForKey() will not be automatically be freed, the
     * REReleaseMemory() function needs to be called for this memory to be freed!
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function.
     * \return 0 when an error has occured, 1 if loading was successfull. */
    RELibOpt    quint32     RECConv     REDeleteDocument                (void *rdHandle);

    /*! \fn quint32 REGetDocumentStringCount (void const *rdHandle)
     * \brief Retrieve the number of strings associated with a given Recognition Document handle.
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the string count needs to be known.
     * \return The number of strings attached to the given document. */
    RELibOpt    quint32     RECConv     REGetDocumentStringCount        (void const *rdHandle);
    /*! \fn quint32 REGetDocumentKeyAndStringSizesAt (void const *rdHandle, quint32 index, quint32 &keySizeOut, quint32 &valSizeOut)
     * \brief Retrieves the size of the key and value stored in the Recognition Document's strings at a given offset.
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the key and value sizes need to be retrieved.
     * \param[in] index The offset at which the key and values will be looked up.
     * \param[out] keySizeOut An 32-bit unsigned integer that will be set to the size of the key.
     * \param[out] valSizeOut An 32-bit unsigned integer that will be set to the size of the value.
     * \return 0 when an error has occured, 1 if the operation was successfull. */
    RELibOpt    quint32     RECConv     REGetDocumentKeyAndStringSizesAt    (void const *rdHandle, quint32 index, quint32 &keySizeOut, quint32 &valSizeOut);
    /*! \fn quint32 REGetDocumentKeyAndStringAt (void const *rdHandle, quint32 index, char *keyOut, char *valOut)
     * \brief Retrieves a key and a value at a given index in the Recognition Document strings list.
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the key and value need to be retrieved.
     * \param[in] index The offset at which the key and values will be looked up.
     * \param[out] keySizeOut An buffer that will be filled with the contents of the key. The buffer needs to be allocated before calling the function.
     * \param[out] valSizeOut An buffer that will be filled with the contents of the value. The buffer needs to be allocated before calling the function.
     * \return 0 when an error has occured, 1 if the operation was successfull. */
    RELibOpt    quint32     RECConv     REGetDocumentKeyAndStringAt         (void const *rdHandle, quint32 index, char *keyOut, char *valOut);
    /*! \fn quint32 RESetDocumentStringForKey (void const *rdHandle, char const *key, char *out, quint32 maximumStringLength)
     * \brief Attaches a string to a Recognition Document based on a given key.
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the key and value need to be retrieved.
     * \param[in] key The key for which the string will be attached to the document.
     * \param[in] value The string itself.
     * \return 0 when an error has occured, 1 if the operation was successfull. */
    RELibOpt    quint32     RECConv     RESetDocumentStringForKey       (void *rdHandle, char const *key, char const *value);
    /*! \fn quint32 REDeleteDocumentStringForKey (void *rdHandle, char const *key)
     * \brief Deletes an attached string from a Recognition Document based on a given key.
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the string needs to be removed.
     * \param[in] key The key that needs to be used for the string to be deleted.
     * \return 0 when an error has occured, 1 if the operation was successfull. */
    RELibOpt    quint32     RECConv     REDeleteDocumentStringForKey    (void *rdHandle, char const *key);

    /*! \fn quint32 REGetDocumentImageCount (void const *rdHandle)
     * \brief Retrieve the number of images associated with a given Recognition Document handle.
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the image count needs to be known.
     * \return The number of images attached to the given document. */
    RELibOpt    quint32     RECConv     REGetDocumentImageCount         (void const *rdHandle);
    /*! \fn quint32 REGetDocumentImageKeySizeAt (void const *rdHandle, quint32 index, quint32 &keySizeOut)
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the image key size needs to be known.
     * \param[in] index The offset at which the key size will be looked up.
     * \param[out] keySizeOut A 32-bit unsigned integer that will be set to the size of the key.
     * \return 0 when an error has occured, 1 if the operation was successfull. */
    RELibOpt    quint32     RECConv     REGetDocumentImageKeySizeAt    (void const *rdHandle, quint32 index, quint32 &keySizeOut);
    /*! \fn quint32 REGetDocumentImageKeyAt (void const *rdHandle, quint32 index, char *out)
     * \brief Retrieves a Recognition Document's image key for a given document handle and index.
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the image key needs to be known.
     * \param[in] index The offset at which the key will be looked up.
     * \param[out] outKey A char pointer for the result to be put in. The image needs to be previously allocated.
     * \return 0 when an error has occured, 1 if the operation was successfull. */
    RELibOpt    quint32     RECConv     REGetDocumentImageKeyAt         (void const *rdHandle, quint32 index, char *outKey);
    /*! \fn quint32 REGetDocumentImageForKey (void const *rdHandle, char const *key, char *out, quint32 maximumStringLength)
     * \brief Retrieves a Recognition Document's image for a given key.
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the image needs to be retrieved.
     * \param[in] key The key for which the image will be retrieved.
     * \params[in] format Can be of'jpg', 'png', 'gif', 'bmp'.
     * \params[in] quality Not yet documented.
     * \params[out] outImageMemoryHandle A pointer to a pointer for the image buffer to be allocated and filled with data.
     * \return 0 when an error has occured, any other value will be the length of the allocated outImageMemoryHandle. */
    RELibOpt    quint32     RECConv     REGetDocumentImageForKey    (void const *rdHandle, char const *key, char const *format, quint32 quality, quint64 &outLength, quint8 **outImageMemoryHandle);
    /*! \fn quint32 REReleaseMemory (quint8 **imageMemoryHandle)
     * \brief Deletes memory previously allocated by the REGetDocumentImageForKey() function.
     * \param[in] imageMemoryHandle The memory block allocated by REGetDocumentImageForKey().
     * \return 0 when an error has occured, 1 if the operation was successfull. */
    RELibOpt    quint32     RECConv     REReleaseMemory                 (quint8 **imageMemoryHandle);
    /*! \fn quint32 RESetDocumentImageForKey (void *rdHandle, char const *key, char const *data, quint32 dataLength)
     * \brief Attaches an image to a Recognition Document based on a given key.
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the image needs to be set.
     * \param[in] key The key that needs to be used for the image to be set.
     * \param[in] data The image data to be processed and attached to the Recognition Document./
     * \param[in] dataLength The length of the data to be read.
     * \return 0 when an error has occured, 1 if the operation was successfull. */
    RELibOpt    quint32     RECConv     RESetDocumentImageForKey        (void *rdHandle, char const *key, char const *data, quint32 dataLength);
    /*! \fn quint32 REDeleteDocumentImageForKey (void *rdHandle, char const *key)
     * \brief Deletes an attached image from a Recognition Document based on a given key.
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the image needs to be removed.
     * \param[in] key The key that needs to be used for the image to be deleted.
     * \return 0 when an error has occured, 1 if the operation was successfull. */
    RELibOpt    quint32     RECConv     REDeleteDocumentImageForKey     (void *rdHandle, char const *key);

    /*! \fn quint32 REGuessDocumentType (void const *reHandle, void *rdHandle)
     * \brief Guesses a Recognition Document's type based on it's attached strings and images.
     * \param[in] reHandle A Recognition Engine handle previously allocated with the RECreateEngine() function.
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and on which the guess will be taken.
     * \param[in] parallelizedCoresCount An unsigned int telling the engine how many CPU cores to use when filtering the image. If unsure, use 1. If you want to use the ideal CPU count, pass 0.
     * \return 0 when an error has occured, 1 if the operation was successfull. */
    RELibOpt    quint32     RECConv     REGuessDocumentType             (void const *reHandle, void *rdHandle, quint32 parallelizedCoresCount);
    /*! \fn quint32 REGetDocumentType (void const *rdHandle)
     * \brief Retrieves a Recognition Document's type index. This call is mostly usefull after using the REGuessDocumentType() function.
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the type index needs to be known.
     * \return 0 when an error has occured or if the Recognition Document's type is unknown, any other value for it's type. */
    RELibOpt    quint32     RECConv     REGetDocumentType               (void const *rdHandle);
    /*! \fn quint32 RESetDocumentType (void const *rdHandle)
     * \brief Forces a Recognition Document's type by it's index. This function's main purpose is to bypass the neural network
     * based document type recognition process when the document type is known.
     * \param[in] reHandle A Recognition Engine handle previously allocated with the RECreateEngine() function. The client binary data loaded into this engine will be used for setting the document type index.
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the type index needs to be known.
     * \param[in] typeCode The document type code to be set for the given document handle. This document type code has to exist in the client binary documents definition logic.
     * \return 0 when an error has occured or if the Recognition Document's type is unknown, any other value for it's type. */
    RELibOpt    quint32     RECConv     RESetDocumentType               (void const *reHandle, void *rdHandle, quint32 typeCode);
    /*! \fn quint32 RERecognizeDocumentData (void const *reHandle, void *rdHandle)
     * \brief Starts the recognition on a Recognition Document. Once the recognition has been achieved, the results will be made available in the Recognition Document's strings and images.
     * \param[in] reHandle A Recognition Engine handle previously allocated with the RECreateEngine() function.
     * \param[in] rdHandle A handle to a Recognition Document previously instanciated with the RECreateDocument() function and for which the recognition process needs to be done.
     * \param[in] parallelizedCoresCount An unsigned int telling the engine how many CPU cores to use when filtering the image. If unsure, use 1. If you want to use the ideal CPU count, pass 0.
     * \param[in] threadId A thread identifier from the caller's context, or zero is no multi-threading is required.
     * \return 0 when an error has occured, 1 if the operation was successfull. */
    RELibOpt    quint32     RECConv     RERecognizeDocumentData         (void const *reHandle, void *rdHandle, quint32 parallelizedCoresCount, quint32 threadId);
}

#endif
